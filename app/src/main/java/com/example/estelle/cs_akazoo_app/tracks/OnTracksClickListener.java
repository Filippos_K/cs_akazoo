package com.example.estelle.cs_akazoo_app.tracks;

public interface OnTracksClickListener {

    void onTrackClicked(Track track);

    void onTrackLogoClick(Track track);
}
