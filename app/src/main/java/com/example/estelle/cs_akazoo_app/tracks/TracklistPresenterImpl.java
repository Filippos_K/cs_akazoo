package com.example.estelle.cs_akazoo_app.tracks;

import java.util.ArrayList;

public class TracklistPresenterImpl implements TracklistPresenter {

    TracksView tracklistView;

    public TracklistPresenterImpl(TracksView tracklistView) {
        this.tracklistView = tracklistView;
    }

    @Override
    public void getTracklists() {

        ArrayList tracklist = addMockTracks();

        tracklistView.showTracklist(tracklist);

    }


    private ArrayList<Track> addMockTracks() {
        ArrayList<Track> tracks = new ArrayList<Track>();
        for (int i = 1; i < 100; i++) {
            Track track = new Track("Track Name " + i, "Track Artist " + i, "TheCategory " + i);
            tracks.add(track);
        }
        return tracks;
    }
}
